import { Component, Input, NgZone, OnInit } from '@angular/core';
import { ClockInfos }                       from '../../../Models/Time/clock-infos';
import { WidgetService }                    from '../../../Services/WidgetsService/widget.service';

@Component({
             selector:    'app-widget-selector',
             templateUrl: './widget-selector.component.html',
             styleUrls:   ['./widget-selector.component.css'],
           })
export class WidgetSelectorComponent implements OnInit {

  @Input('target-widget') _target: string;

  private _widget: any;

  constructor(private _ngZone: NgZone, private wm: WidgetService) {
  }

  ngOnInit() {
  }

  get target() {
    return this._target;
  }

  createComponent(componentType: string) {
    this.wm.put(componentType);
  }

}

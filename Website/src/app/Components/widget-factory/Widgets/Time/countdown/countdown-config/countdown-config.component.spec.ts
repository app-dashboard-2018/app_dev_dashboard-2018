import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountdownConfigComponent } from './countdown-config.component';

describe('CountdownConfigComponent', () => {
  let component: CountdownConfigComponent;
  let fixture: ComponentFixture<CountdownConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountdownConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountdownConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, NgZone, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ClockService }                                               from '../../../../../Services/DashboardServices/ClockService/clock.service';
import { WidgetService }                                              from '../../../../../Services/WidgetsService/widget.service';
import { WidgetInfos }                                                from '../../../../../Models/widget-infos';
import { Vector }                                                     from '../../../../../Models/Vector';
import { CountdownInfos }                                             from '../../../../../Models/Time/countdown-infos';
import { CountdownConfigComponent }                                   from './countdown-config/countdown-config.component';

export interface DateInfos {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}

@Component({
             selector:    'app-countdown',
             templateUrl: './countdown.component.html',
             styleUrls:   ['./countdown.component.css'],
           })
export class CountdownComponent implements OnInit, OnChanges {
  private inited: boolean = false;

  @Input('options') _options: CountdownInfos;

  constructor(private _clockService: ClockService, private _zone: NgZone, private _wm: WidgetService) {
  }

  ngOnInit() {
    this._options.onChangeEndDate = (value: Date) => {
      this._zone.run(() => {
        this._options.endDate = value;
        this._wm.persist(this._options.uuid);
      });
    };
    this._options.onTitleChange   = (value: string) => {
      this._zone.run(() => {
        this._options.title = value;
        this._wm.persist(this._options.uuid);
      });
    };
    this.inited                   = true;
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  get widgetInfos(): WidgetInfos {
    return this._options;
  }

  get date(): DateInfos {
    const ret: DateInfos = {days: 0, hours: 0, minutes: 0, seconds: 0};

    if (this._options.endDate) {
      let dist = this._options.endDate.getTime() - this._clockService.now.getTime();
      if (dist < 0) {
        dist *= -1;
      }
      ret.days    = Math.round(dist / (1000 * 60 * 60 * 24));
      ret.hours   = Math.round((dist % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      ret.minutes = Math.round((dist % (1000 * 60 * 60)) / (1000 * 60));
      ret.seconds = Math.round((dist % (1000 * 60)) / 1000);
    }
    return ret;
  }

  get positive(): boolean {
    if (this._options.endDate) {
      return (this._options.endDate.getTime() - this._clockService.now.getTime()) < 0;
    }
    return false;
  }

  get days(): string {
    return ('00' + this.date.days).slice(-2);
  }

  get hours(): string {
    return ('00' + this.date.hours).slice(-2);
  }

  get minutes(): string {
    return ('00' + this.date.minutes).slice(-2);
  }

  get seconds(): string {
    return ('00' + this.date.seconds).slice(-2);
  }

  get position(): Vector {
    return this._options.position;
  }

  set position(value: Vector) {
    this._options.position = value;
  }

  get size(): Vector {
    return this._options.size;
  }

  get configModal(): typeof CountdownConfigComponent {
    return CountdownConfigComponent;
  }

  get title(): string {
    return this._options.title;
  }
}

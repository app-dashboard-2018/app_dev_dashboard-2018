import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup }    from '@angular/forms';
import { MAT_DIALOG_DATA }           from '@angular/material';
import { CountdownInfos }            from '../../../../../../Models/Time/countdown-infos';
// @ts-ignore
import Timer = NodeJS.Timer;

@Component({
             selector:    'app-countdown-config',
             templateUrl: './countdown-config.component.html',
             styleUrls:   ['./countdown-config.component.css'],
           })
export class CountdownConfigComponent implements OnInit {
  private _delayUpdate: Timer                = null;
  public countdownConfig                     = new FormGroup({
                                                               title:   new FormControl(''),
                                                               date:    new FormControl(new Date()),
                                                               hours:   new FormControl((new Date()).getHours()),
                                                               minutes: new FormControl((new Date()).getMinutes()),
                                                               seconds: new FormControl((new Date()).getSeconds()),
                                                             });
  private _changes: { [k: string]: boolean } = {
    title:   false,
    date:    false,
    hours:   false,
    minutes: false,
    seconds: false,
  };

  constructor(@Inject(MAT_DIALOG_DATA) public data: CountdownInfos) {
    this.countdownConfig.valueChanges.subscribe((val) => {
      if (this._delayUpdate) {
        clearTimeout(this._delayUpdate);
      }

      this._delayUpdate = setTimeout(() => {
        const date = new Date(val.date.getFullYear(), val.date.getMonth(), val.date.getDate(), val.hours, val.minutes, val.seconds,
                              0);
        if (this.data.onChangeEndDate) {
          this.data.onChangeEndDate(date);
        }
      }, 300);
    });
  }

  ngOnInit() {
    if (this.data.endDate) {
      const date = this.data.endDate;

      this.countdownConfig.patchValue({date: date, hours: date.getHours(), minutes: date.getMinutes(), seconds: date.getSeconds()},
                                      {emitEvent: false});
    }
  }

  array(indexes: number): number[] {
    return Array.from(Array(indexes).keys());
  }

}

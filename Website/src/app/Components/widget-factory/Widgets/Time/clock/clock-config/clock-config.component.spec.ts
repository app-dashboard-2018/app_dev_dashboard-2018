import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClockConfigComponent } from './clock-config.component';

describe('ClockConfigComponent', () => {
  let component: ClockConfigComponent;
  let fixture: ComponentFixture<ClockConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClockConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClockConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, NgZone, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ClockInfos }                                                 from '../../../../../Models/Time/clock-infos';
import { Vector }                                                     from '../../../../../Models/Vector';
import { WidgetInfos }                                                from '../../../../../Models/widget-infos';
import { ClockConfigComponent }                                       from './clock-config/clock-config.component';
import { WidgetService }                                              from '../../../../../Services/WidgetsService/widget.service';
import { ClockService }                                               from '../../../../../Services/DashboardServices/ClockService/clock.service';

@Component({
             selector:    'app-clock',
             templateUrl: './clock.component.html',
             styleUrls:   ['./clock.component.css'],
           })
export class ClockComponent implements OnInit, OnChanges {
  private inited: boolean = false;

  @Input('options') _options: ClockInfos;

  constructor(private _clockService: ClockService, private _zone: NgZone, private _wm: WidgetService) {
  }

  ngOnInit() {
    this._options.onToggleDate  = (value: boolean) => {
      this._zone.run(() => {
        this._options.showDate = value;
        this._wm.persist(this._options.uuid);
      });
    };
    this._options.onTitleChange = (value: string) => {
      this._zone.run(() => {
        this._options.title = value;
        this._wm.persist(this._options.uuid);
      });
    };
    this.inited                 = true;
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  get widgetInfos(): WidgetInfos {
    return this._options;
  }

  get hours(): string {
    return ('00' + this._clockService.now.getHours()).slice(-2);
  }

  get minutes(): string {
    return ('00' + this._clockService.now.getMinutes()).slice(-2);
  }

  get seconds(): string {
    return ('00' + this._clockService.now.getSeconds()).slice(-2);
  }

  get date(): Date {
    return this._clockService.now;
  }

  get position(): Vector {
    return this._options.position;
  }

  set position(value: Vector) {
    this._options.position = value;
  }

  get size(): Vector {
    return this._options.size;
  }

  get showDate(): boolean {
    return this._options.showDate;
  }

  get configModal(): typeof ClockConfigComponent {
    return ClockConfigComponent;
  }

  get title(): string {
    return this._options.title;
  }
}

import { Component, Inject, OnInit }          from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA }                    from '@angular/material';
import { ClockInfos }                         from '../../../../../../Models/Time/clock-infos';
// @ts-ignore
import Timer = NodeJS.Timer;

@Component({
             selector:    'app-clock-config',
             templateUrl: './clock-config.component.html',
             styleUrls:   ['./clock-config.component.css'],
           })
export class ClockConfigComponent implements OnInit {

  private _delayDate: Timer = null;
  private _delayName: Timer = null;
  public clockConfig: FormGroup = new FormGroup({
                                                  date:        new FormControl(true, [Validators.required]),
                                                  widget_name: new FormControl('', [
                                                    Validators.minLength(0),
                                                    Validators.maxLength(24),
                                                  ]),
                                                });

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClockInfos) {
    this.clockConfig.get('date').valueChanges.subscribe((val) => {
      if (this._delayDate) {
        clearTimeout(this._delayDate);
        this._delayName = null;
      }
      this._delayName = setTimeout(() => {
        data.onToggleDate(val);
      }, 300);
    });
    this.clockConfig.get('widget_name').valueChanges.subscribe((val) => {
      if (this._delayName) {
        clearTimeout(this._delayName);
        this._delayName = null;
      }
      this._delayName = setTimeout(() => {
        data.onTitleChange(val);
      }, 300);
    });
  }

  ngOnInit() {
    this.clockConfig.get('date').patchValue(this.data.showDate, {emitEvent: false});
    this.clockConfig.get('widget_name').patchValue(this.data.title, {emitEvent: false});

  }

}

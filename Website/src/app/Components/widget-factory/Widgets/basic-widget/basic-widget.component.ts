import { Component, EventEmitter, Input, NgZone, OnInit, Output } from '@angular/core';
import { Vector }                                                 from '../../../../Models/Vector';
import { WidgetService }                                          from '../../../../Services/WidgetsService/widget.service';
import { Layout }                                                 from '../../../../Models/layout';
import { WidgetInfos }                                            from '../../../../Models/widget-infos';
import { MatDialog }                                              from '@angular/material';
// @ts-ignore
import Timer = NodeJS.Timer;

@Component({
             selector:    'app-basic-widget',
             templateUrl: './basic-widget.component.html',
             styleUrls:   ['./basic-widget.component.css'],
           })
export class BasicWidgetComponent implements OnInit {

  @Input('label') _title: string;
  @Input('size') private _size: Vector                                     = new Vector(4, 6);
  @Input('position') private _position: Vector                             = new Vector(1, 1);
  @Output('positionChange') private _positionEmitter: EventEmitter<Vector> = new EventEmitter<Vector>();
  @Input('widgetInfos') private _widgetInfos: WidgetInfos;
  @Input('configModal') private _configModal: any                          = null;
  public edit: boolean                                                     = false;
  private _delay: Timer                                                    = null;

  constructor(private dialog: MatDialog, private _wm: WidgetService, private _ngZone: NgZone) {
  }

  ngOnInit() {
  }

  get configurable(): boolean {
    return this._configModal ? true : false;
  }

  get title(): string {
    return this._title;
  }

  get size(): Vector {
    return this._size;
  }

  get position(): Vector {
    return this._position;
  }

  get layout(): Layout {
    return this._wm.layout;
  }

  get width() {
    return this.size.posX * this.layout.gridSizeX;
  }

  get height() {
    return this.size.posY * this.layout.gridSizeY;
  }

  deleteComponent() {
    this._wm.delete(this._widgetInfos.uuid);
  }

  settings() {
    this.dialog.open(this._configModal, {data: this._widgetInfos});
  }

  drag(evt) {
    this._ngZone.run(
      () => {
        evt.x -= evt.x % this.layout.gridSizeX;
        evt.y -= evt.y % this.layout.gridSizeY;
        const posX = this._position.posX + evt.x / this.layout.gridSizeX,
              posY = this._position.posY + evt.y / this.layout.gridSizeY;
        const maxX = Math.floor(window.innerWidth / this.layout.gridSizeX - this.size.posX),
              maxY = Math.floor(window.innerHeight / this.layout.gridSizeY - this.size.posY);
        const x = (posX < 0) ? 0 : (posX > maxX) ? maxX : posX;
        const y = (posY < 0) ? 0 : (posY > maxY) ? maxY : posY;

        this._position.posX = x;
        this._position.posY = y;

        this._positionEmitter.emit(this._position);
        this.edit = false;
        if (this._delay) {
          clearTimeout(this._delay);
          this._delay = null;
        }
        this._delay = setTimeout(() => {
          this._wm.persist(this._widgetInfos.uuid);
        }, 300);

      },
    );
  }
}

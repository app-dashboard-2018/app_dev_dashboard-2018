import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherOneDayConfigComponent } from './weather-one-day-config.component';

describe('WeatherOneDayConfigComponent', () => {
  let component: WeatherOneDayConfigComponent;
  let fixture: ComponentFixture<WeatherOneDayConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherOneDayConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherOneDayConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit }          from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA }                    from '@angular/material';
import { WeatherInfos }                       from '../../../../../../Models/Weather/weather-infos';

@Component({
             selector:    'app-weather-one-day-config',
             templateUrl: './weather-one-day-config.component.html',
             styleUrls:   ['./weather-one-day-config.component.css'],
           })
export class WeatherOneDayConfigComponent implements OnInit {

  public weatherOneDayConfig: FormGroup = new FormGroup({
                                                          extremum: new FormControl(true, [Validators.required]),
                                                          city:     new FormControl('Paris', [
                                                            Validators.required,
                                                            Validators.minLength(0),
                                                          ]),
                                                        });

  constructor(@Inject(MAT_DIALOG_DATA) public data: WeatherInfos) {
    this.weatherOneDayConfig.valueChanges.subscribe((val) => {
      if (data.onChangeCity) {
        data.onChangeCity(val.city);
      }
      if (data.onToggleExtremum) {
        data.onToggleExtremum(val.extremum);
      }
    });
  }

  ngOnInit() {
    this.weatherOneDayConfig.patchValue({extremum: this.data.extremum, city: this.data.cityName}, {emitEvent: false});

  }

}

import { Component, Input, NgZone, OnInit } from '@angular/core';
import { WeatherInfos }                     from '../../../../../Models/Weather/weather-infos';
import { WeatherOneDayConfigComponent }     from './weather-one-day-config/weather-one-day-config.component';
import { Vector }                           from '../../../../../Models/Vector';
import { WidgetService }                    from '../../../../../Services/WidgetsService/widget.service';
import { WidgetInfos }                      from '../../../../../Models/widget-infos';

@Component({
             selector:    'app-weather-one-day',
             templateUrl: './weather-one-day.component.html',
             styleUrls:   ['./weather-one-day.component.css'],
           })
export class WeatherOneDayComponent implements OnInit {

  @Input('options') private _options: WeatherInfos;
  public weatherIcon: string = 'assets/sun.svg';

  constructor(private _zone: NgZone, private _wm: WidgetService) {
  }

  ngOnInit() {
    this._options.onToggleExtremum = (value: boolean) => {
      this._zone.run(() => {
        this._options.extremum = value;
        this._wm.persist(this._options.uuid);
      });
    };
    this._options.onChangeCity     = (value: string) => {
      this._zone.run(() => {
        this._options.cityName = value;
        this._wm.persist(this._options.uuid);
      });
    };
    this._options.onTitleChange    = (value: string) => {
      this._zone.run(() => {
        this._options.title = value;
        if (value !== '') {
          this._options.size.posY = 3;
        } else {
          this._options.size.posY = 2;
        }
        this._wm.persist(this._options.uuid);
      });
    };
  }

  get widgetInfos(): WidgetInfos {
    return this._options;
  }

  get position(): Vector {
    return this._options.position;
  }

  set position(value: Vector) {
    this._options.position = value;
  }

  get size(): Vector {
    return this._options.size;
  }

  get configModal(): typeof WeatherOneDayConfigComponent {
    return WeatherOneDayConfigComponent;
  }

  get title(): string {
    return this._options.title;
  }

  get city(): string {
    return this._options.cityName;
  }

  get extremum(): { min: number, max: number } {
    return {min: 15, max: 22.6};
  }

  get temperature() {
    return 17.5;
  }
}

import { Component, Input, OnInit }     from '@angular/core';
import { PokedexInfos }                 from '../../../../../Models/Pokemon/pokedex-infos';
import { WidgetInfos }                  from '../../../../../Models/widget-infos';
import { Vector }                       from '../../../../../Models/Vector';
import { FormControl, FormGroup }       from '@angular/forms';
import { PokemonService }               from '../../../../../Services/DashboardServices/PokemonService/pokemon.service';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { Pokemon, PokemonIdentity }     from '../../../../../Models/Pokemon/pokemon';
import { WidgetService }                from '../../../../../Services/WidgetsService/widget.service';
// @ts-ignore
import Timer = NodeJS.Timer;

@Component({
             selector:    'app-pokedex',
             templateUrl: './pokedex.component.html',
             styleUrls:   ['./pokedex.component.css'],
           })
export class PokedexComponent implements OnInit {
  private _pokemonSearchForm: FormGroup = new FormGroup({
                                                          search: new FormControl(''),
                                                        });
  @Input('options') _options: PokedexInfos;
  private _delay: Timer                 = null;
  private _found: boolean               = false;
  private _pokemon: Pokemon             = null;

  constructor(private _pokemonService: PokemonService, private _ws: WidgetService) {
  }

  ngOnInit() {
    if (this._options.pokemonId) {
      this.getPokemon(this._options.pokemonId);
    }
    this._pokemonSearchForm.get('search').valueChanges
        .subscribe((value: string) => {
          if (value.length < 2) {
            return;
          }
          if (this._delay) {
            clearTimeout(this._delay);
            this._delay = null;
          }

          this._delay = setTimeout(() => {
            this._pokemonService.nameSearch(this._options.uuid, value);
          }, 300);
        });
  }

  get pokemonSearchForm(): FormGroup {
    return this._pokemonSearchForm;
  }

  get widgetInfos(): WidgetInfos {
    return this._options;
  }

  get position(): Vector {
    return this._options.position;
  }

  set position(value: Vector) {
    this._options.position = value;
  }

  get size(): Vector {
    return this._options.size;
  }

  get configModal() {
    return null;
  }

  get pokemonSearchResults(): PokemonIdentity[] {
    return this._pokemonService.getSearch(this._options.uuid);
  }

  get pokemonFound(): boolean {
    return this._found;
  }

  get pokemon(): Pokemon {
    return this._pokemon;
  }

  get pokemonTypes(): string {
    return this.pokemon.types.join(' & ');
  }

  get title(): string {
    return this._options.title;
  }

  getPokemon(pkmnId: number) {
    this._pokemonService.getPokemonInfosById(this._options.uuid, pkmnId)
        .then((res: Pokemon) => {
          this._pokemon = res;
          this._found   = !!res;
          this._options.pokemonId = res.identity.id;
          this._ws.persist(this._options.uuid);
        })
        .catch((err) => {
          console.error(err);
        });
  }
}

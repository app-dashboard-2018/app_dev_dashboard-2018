import { Component, Inject, NgZone, OnInit }  from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef }      from '@angular/material';
import { PokeTeamInfos }                      from '../../../../../../Models/Pokemon/poke-team-infos';
import { PokemonService }                     from '../../../../../../Services/DashboardServices/PokemonService/pokemon.service';
import { PokemonIdentity }                    from '../../../../../../Models/Pokemon/pokemon';
import { WidgetService }                      from '../../../../../../Services/WidgetsService/widget.service';
// @ts-ignore
import Timer = NodeJS.Timer;

@Component({
             selector:    'app-poke-team-config',
             templateUrl: './poke-team-config.component.html',
             styleUrls:   ['./poke-team-config.component.css'],
           })
export class PokeTeamConfigComponent implements OnInit {
  private _delay: Timer                  = null;
  private _delayPersist: Timer           = null;
  public poketeam: FormGroup             = new FormGroup({
                                                           team_name: new FormControl('', [Validators.minLength(2)]),
                                                           search:    new FormControl('', [Validators.minLength(2)]),
                                                         });
  private _pokelist: PokemonIdentity[]   = [];
  private _pokesearch: PokemonIdentity[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: PokeTeamInfos,
              public dialogRef: MatDialogRef<PokeTeamConfigComponent>,
              private _pokemonService: PokemonService,
              private _zone: NgZone,
              private _wm: WidgetService) {
  }

  ngOnInit() {
    this.poketeam.get('search').valueChanges
        .subscribe((value: string) => {
          if (value.length < 2) {
            return;
          }
          if (this._delay) {
            clearTimeout(this._delay);
            this._delay = null;
          }

          this._delay = setTimeout(() => {
            this.getPokemon(value);
          }, 300);
        });
  }

  get pokelist(): PokemonIdentity[] {
    return this.data.pokelist;
  }

  get pokemonSearchResults(): PokemonIdentity[] {
    return this._pokesearch;
  }

  getPokemon(value: string) {
    this._pokemonService.nameSearchPromise(value)
        .then((resp) => {
          this._pokesearch = resp;
        });
  }

  get searchValue(): string {
    return this.poketeam.get('search').value;
  }

  set searchValue(value: string) {
    this.poketeam.get('search').patchValue(value, {emitEvent: false});
  }

  get foundPokemon(): PokemonIdentity {
    return this._pokesearch.find((item) => {
      return item.english === this.searchValue || item.french === this.searchValue || item.deutsch === this.searchValue;
    });
  }

  get isSearchIsPokemon(): boolean {
    return !!this.foundPokemon;
  }

  private persist() {
    if (this._delayPersist) {
      clearTimeout(this._delayPersist);
      this._delayPersist = null;
    }
    this._delayPersist = setTimeout(() => {
      this._wm.persist(this.data.uuid);
    }, 300);
  }

  public removePokemon(index: number) {
    this.data.pokelist.splice(index, 1);
    this.persist();
  }

  public addPokemon() {
    this.data.pokelist.push(this.foundPokemon);
    this.searchValue = '';
    this.persist();
  }
}

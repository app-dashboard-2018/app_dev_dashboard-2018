import { Component, Input, OnInit }           from '@angular/core';
import { LoginService }                       from '../../../../Services/AuthServices/Login/login.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService }                        from '../../../../Services/AuthServices/User/user.service';
import { MatDialogRef }                       from '@angular/material';
import { LogboxComponent }                    from '../logbox.component';
import { HttpResponse }                       from '@angular/common/http';

@Component({
             selector:    'app-signup',
             templateUrl: './signup.component.html',
             styleUrls:   ['./signup.component.css'],
           })
export class SignupComponent implements OnInit {

  public _form: FormGroup = new FormGroup({
                                            'username': new FormControl('', [Validators.required]),
                                            'mail':     new FormControl('', [Validators.required]),
                                            'password': new FormControl('', [Validators.required]),
                                          });

  @Input('dialogRef') dialogRef: MatDialogRef<LogboxComponent>;

  constructor(private _loginManager: LoginService,
              private _userService: UserService) {
  }

  ngOnInit() {
  }

  signup() {
    this._loginManager.signup(this._form.value).add((response: HttpResponse<any>) => {
      this._userService.refresh().add(() => {
        this.dialogRef.close();
      });
    });
  }
}

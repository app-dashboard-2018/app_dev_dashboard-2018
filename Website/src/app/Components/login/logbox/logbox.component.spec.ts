import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogboxComponent } from './logbox.component';

describe('LogboxComponent', () => {
  let component: LogboxComponent;
  let fixture: ComponentFixture<LogboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit }     from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
             selector:    'app-logbox',
             templateUrl: './logbox.component.html',
             styleUrls:   ['./logbox.component.css'],
           })
export class LogboxComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<LogboxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}

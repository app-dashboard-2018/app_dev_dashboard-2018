import { Injectable, NgModule }                                                                  from '@angular/core';
import { CommonModule }                                                                          from '@angular/common';
import { HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable }                                                                            from 'rxjs';
import { tap }                                                                                   from 'rxjs/operators';
import { TokenService }                                                                          from '../../Services/AuthServices/Token/token.service';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  constructor(private _ts: TokenService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloned = req;
    let tkn = this._ts.getToken();
    if (tkn) {
      cloned = req.clone({headers: req.headers.set('Authorization', tkn.token)});
    }
    return next.handle(cloned)
               .pipe(
                 tap(evt => {
                   if (evt instanceof HttpResponse) {
                   }
                 }),
               );
  }
}


@NgModule({
            providers:    [
              {provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true},
            ],
            imports:      [
              CommonModule,
            ],
            declarations: [],
          })
export class InterceptorModule {
}

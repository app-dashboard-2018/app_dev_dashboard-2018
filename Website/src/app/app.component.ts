import { Component, OnInit } from '@angular/core';
import { WidgetService }     from './Services/WidgetsService/widget.service';
import { ClockInfos }     from './Models/Time/clock-infos';
import { PokedexInfos }   from './Models/Pokemon/pokedex-infos';
import { UserService }    from './Services/AuthServices/User/user.service';
import { LoginService }   from './Services/AuthServices/Login/login.service';
import { TokenService }   from './Services/AuthServices/Token/token.service';
import { WeatherInfos }   from './Models/Weather/weather-infos';
import { PokeTeamInfos }  from './Models/Pokemon/poke-team-infos';
import { CountdownInfos } from './Models/Time/countdown-infos';

@Component({
             selector:    'app-root',
             templateUrl: './app.component.html',
             styleUrls:   ['./app.component.css'],
           })
export class AppComponent implements OnInit {
  title = 'Dashboard-webapp';

  constructor(private wm: WidgetService,
              private _userService: UserService,
              private _loginManager: LoginService,
              private _tknService: TokenService) {
  }

  ngOnInit() {
    if (this._loginManager.logged) {
      this._userService.refresh();
    }
  }

  get username(): string {
    if (this._loginManager.logged) {
      return this._userService.username;
    } else {
      return 'Invité';
    }
  }

  get clocks(): ClockInfos[] {
    return this.wm.clocks;
  }

  get pokedexs(): PokedexInfos[] {
    return this.wm.pokedex;
  }

  get weatherOneDay(): WeatherInfos[] {
    return this.wm.weatherOneDay;
  }

  get pokeTeams(): PokeTeamInfos[] {
    return this.wm.pokeTeams;
  }

  get countdowns(): CountdownInfos[] {
    return this.wm.countDowns;
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';

import { AppComponent }                               from './app.component';
import { LogboxComponent }                            from './Components/login/logbox/logbox.component';
import { SettingsComponent }                          from './Components/settings/settings.component';
import { SignupComponent }                            from './Components/login/logbox/signup/signup.component';
import { SigninComponent }                            from './Components/login/logbox/signin/signin.component';
import { BrowserAnimationsModule }                    from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
}                                                     from '@angular/material';
import { HTTP_INTERCEPTORS, HttpClientModule }        from '@angular/common/http';
import { FormsModule, ReactiveFormsModule }           from '@angular/forms';
import { LoginComponent }                             from './Components/login/login.component';
import { WidgetListComponent }                        from './Components/widget-factory/widget-list/widget-list.component';
import { WidgetSelectorComponent }                    from './Components/widget-factory/widget-selector/widget-selector.component';
import { ClockComponent }                             from './Components/widget-factory/Widgets/Time/clock/clock.component';
import { BasicWidgetComponent }                       from './Components/widget-factory/Widgets/basic-widget/basic-widget.component';
import { DragAndDropModule }                          from 'angular-draggable-droppable';
import { ClockConfigComponent }                       from './Components/widget-factory/Widgets/Time/clock/clock-config/clock-config.component';
import { PokedexComponent }                           from './Components/widget-factory/Widgets/Pokemon/pokedex/pokedex.component';
import { HttpsRequestInterceptor, InterceptorModule } from './Modules/interceptor-module/interceptor-module';
import { WeatherOneDayComponent }       from './Components/widget-factory/Widgets/Weather/weather-one-day/weather-one-day.component';
import { WeatherOneDayConfigComponent } from './Components/widget-factory/Widgets/Weather/weather-one-day/weather-one-day-config/weather-one-day-config.component';
import { PokeTeamComponent }            from './Components/widget-factory/Widgets/Pokemon/pokemonTeam/poke-team.component';
import { PokeTeamConfigComponent }      from './Components/widget-factory/Widgets/Pokemon/pokemonTeam/poke-team-config/poke-team-config.component';
import { CountdownComponent } from './Components/widget-factory/Widgets/Time/countdown/countdown.component';
import { CountdownConfigComponent } from './Components/widget-factory/Widgets/Time/countdown/countdown-config/countdown-config.component';

@NgModule({
            declarations:    [
              AppComponent,
              LogboxComponent,
              SettingsComponent,
              SignupComponent,
              SigninComponent,
              LoginComponent,
              WidgetListComponent,
              WidgetSelectorComponent,
              ClockComponent,
              BasicWidgetComponent,
              ClockConfigComponent,
              PokedexComponent,
              WeatherOneDayComponent,
              WeatherOneDayConfigComponent,
              PokeTeamComponent,
              PokeTeamConfigComponent,
              CountdownComponent,
              CountdownConfigComponent,
            ],
            entryComponents: [
              LoginComponent,
              LogboxComponent,
              ClockConfigComponent,
              WeatherOneDayConfigComponent,
              PokeTeamConfigComponent,
              CountdownConfigComponent,
            ],
            imports:         [
              BrowserModule,
              BrowserAnimationsModule,
              FormsModule,
              HttpClientModule,
              MatNativeDateModule,
              ReactiveFormsModule,
              MatAutocompleteModule,
              MatBadgeModule,
              MatBottomSheetModule,
              MatButtonModule,
              MatButtonToggleModule,
              MatCardModule,
              MatCheckboxModule,
              MatChipsModule,
              MatDatepickerModule,
              MatDialogModule,
              MatDividerModule,
              MatExpansionModule,
              MatGridListModule,
              MatIconModule,
              MatInputModule,
              MatListModule,
              MatMenuModule,
              MatNativeDateModule,
              MatPaginatorModule,
              MatProgressBarModule,
              MatProgressSpinnerModule,
              MatRadioModule,
              MatRippleModule,
              MatSelectModule,
              MatSidenavModule,
              MatSliderModule,
              MatSlideToggleModule,
              MatSnackBarModule,
              MatSortModule,
              MatStepperModule,
              MatTableModule,
              MatTabsModule,
              MatToolbarModule,
              MatTooltipModule,
              MatTreeModule,
              DragAndDropModule,
              InterceptorModule,
            ],
            providers:       [{provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true}],
            bootstrap:       [AppComponent],
          })
export class AppModule {
}


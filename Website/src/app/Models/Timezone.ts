import { Time } from '@angular/common';
import timezones from 'google-timezones-json';

const TIMEZONES = [
];


export class Timezone {
  private name: string;
  private time: Time;

  constructor(timeFromName: string) {
    this.name = "UTC +1";
    this.time.hours = 1;
    this.time.minutes = 0;

  }
}

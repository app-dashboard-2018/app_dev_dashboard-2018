import { Vector } from './Vector';

export class Layout {
  private _gridSize: Vector = new Vector(75, 75);

  constructor() {

  }

  get gridSizeX(): number {
    return this._gridSize.posX;
  }

  get gridSizeY(): number {
    return this._gridSize.posY;
  }

  set gridSizeX(value: number) {
    this._gridSize.posX = value;
  }

  set gridSizeY(value: number) {
    this._gridSize.posY = value;
  }
}

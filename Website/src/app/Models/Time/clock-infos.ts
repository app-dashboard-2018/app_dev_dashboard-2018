import { WidgetInfos }             from '../widget-infos';
import { HttpResponseUserWidgets } from '../../Services/AuthServices/User/user.service';

export class ClockInfos extends WidgetInfos {
  private _showDate: boolean             = false;
  public onToggleDate: (boolean) => void = null;

  constructor() {
    super();
  }

  get showDate(): boolean {
    return this._showDate;
  }

  set showDate(value: boolean) {
    this._showDate = value;
  }

  set title(value: string) {
    this._title = value;
  }

  get title(): string {
    return this._title;
  }

  public fromHttpWidgetModel(model: HttpResponseUserWidgets): ClockInfos {
    const widget: WidgetInfos = super.fromHttpWidgetModel(model);

    this.uuid = widget.uuid;
    this.position = widget.position;
    this.title = widget.title;

    if (model.options) {
      const opt = JSON.parse(model.options);
      if (opt.showDate) {
        this._showDate = opt.showDate;
      }
    }
    return this;
  }

  public getHttpWidgetModel(): HttpResponseUserWidgets {
    const data          = super.getHttpWidgetModel();
    const customOptions = {
      showDate: this._showDate,
    };

    if (data.options) {
      data.options = JSON.stringify(Object.assign(JSON.parse(data.options), customOptions));
    }

    data.widget = {
      id:          1,
      name:        undefined,
      description: undefined,
      options:     JSON.stringify(customOptions),
      createdAt:   undefined,
      updatedAt:   undefined,
      service:     undefined,
    };
    return data;
  }

}

import { HttpResponseUserWidgets } from '../../Services/AuthServices/User/user.service';
import { WidgetInfos }             from '../widget-infos';

export class CountdownInfos extends WidgetInfos {
  private _endDate: Date                 = null;
  public onChangeEndDate: (Date) => void = null;

  constructor() {
    super();
  }

  get endDate(): Date {
    return this._endDate;
  }

  set endDate(value: Date) {
    this._endDate = value;
  }

  public fromHttpWidgetModel(model: HttpResponseUserWidgets): CountdownInfos {
    const widget: WidgetInfos = super.fromHttpWidgetModel(model);

    this.uuid     = widget.uuid;
    this.position = widget.position;
    this.title    = widget.title;

    if (model.options) {
      const opt = JSON.parse(model.options);
      if (opt.endDate) {
        this._endDate = new Date(opt.endDate);
      }
    }
    return this;
  }

  public getHttpWidgetModel(): HttpResponseUserWidgets {
    const data          = super.getHttpWidgetModel();
    const customOptions = {
      endDate: this._endDate,
    };

    if (data.options) {
      data.options = JSON.stringify(Object.assign(JSON.parse(data.options), customOptions));
    }

    data.widget = {
      id:          5,
      name:        undefined,
      description: undefined,
      options:     JSON.stringify(customOptions),
      createdAt:   undefined,
      updatedAt:   undefined,
      service:     undefined,
    };
    return data;
  }
}

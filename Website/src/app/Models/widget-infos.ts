import { v4 } from 'uuid';

import { Vector }                  from './Vector';
import { HttpResponseUserWidgets } from '../Services/AuthServices/User/user.service';

export class WidgetInfos {
  get position(): Vector {
    return this._position;
  }

  set position(value: Vector) {
    this._position = value;
  }

  get size(): Vector {
    return this._size;
  }

  set size(value: Vector) {
    this._size = value;
  }

  get uuid(): string {
    return this._uuid;
  }

  set uuid(value: string) {
    this._uuid = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  public onTitleChange: (string) => void = null;

  protected _position: Vector = new Vector(0, 0);
  protected _size: Vector     = new Vector(4, 2);
  protected _title: string    = '';
  protected _uuid: string;

  constructor(uuid: string = v4()) {
    this._uuid = uuid;
  }

  public fromHttpWidgetModel<T extends WidgetInfos>(model: HttpResponseUserWidgets): WidgetInfos {
    const ret = new WidgetInfos();

    if (model.options) {
      const opts = JSON.parse(model.options);

      if (opts.title) {
        ret._title = opts.title;
      }
    }

    ret._position = new Vector(model.positionX, model.positionY);
    ret._uuid = model.uuid;
    ret._size = new Vector(4, 2);
    return ret;
  }

  public getHttpWidgetModel(): HttpResponseUserWidgets {
    const options = {
      title: this._title,
    };
    return {
      positionY: this._position.posY,
      positionX: this._position.posX,
      uuid:      this.uuid,
      options:   JSON.stringify(options),
      createdAt: undefined,
      updatedAt: undefined,
      widget:    undefined,
    };
  }
}

import { WidgetInfos }             from '../widget-infos';
import { HttpResponseUserWidgets } from '../../Services/AuthServices/User/user.service';
import { Vector }                  from '../Vector';

export class PokedexInfos extends WidgetInfos {

  private _pokemonId: number = null;

  constructor() {
    super();
    this.size = new Vector(8, 5);

  }

  get pokemonId(): number {
    return this._pokemonId;
  }

  set pokemonId(value: number) {
    this._pokemonId = value;
  }


  public fromHttpWidgetModel(model: HttpResponseUserWidgets): PokedexInfos {
    const widget: WidgetInfos = super.fromHttpWidgetModel(model);

    this.uuid = widget.uuid;
    this.position = widget.position;
    this.title = widget.title;

    if (model.options) {
      const opt = JSON.parse(model.options);
      if (opt.pokemonId) {
        this.pokemonId = opt.pokemonId;
      }
    }
    return this;
  }

  public getHttpWidgetModel(): HttpResponseUserWidgets {
    const data          = super.getHttpWidgetModel();
    const customOptions = {
      pokemonId: this._pokemonId,
    };

    if (data.options) {
      data.options = JSON.stringify(Object.assign(JSON.parse(data.options) || {}, customOptions));
    }

    data.widget = {
      id:          2,
      name:        undefined,
      description: undefined,
      options:     undefined,
      createdAt:   undefined,
      updatedAt:   undefined,
      service:     undefined,
    };
    return data;
  }
}

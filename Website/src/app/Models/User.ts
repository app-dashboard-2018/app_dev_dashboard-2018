export type UUID = string;

export class User {

  private _uuid: UUID = "0";
  private _username: string = "Invité";
  private _mail: string = "";

  constructor() {
  }

  get uuid(): UUID {
    return this._uuid;
  }

  set uuid(value: UUID) {
    this._uuid = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get mail(): string {
    return this._mail;
  }

  set mail(value: string) {
    this._mail = value;
  }

  public setUuid(value: UUID): this {
    this._uuid = value;
    return this;
  }

  public setUsername(value: string): this {
    this._username = value;
    return this;
  }

  public setMail(value: string): this {
    this._username = value;
    return this;
  }
}

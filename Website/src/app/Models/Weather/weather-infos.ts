import { WidgetInfos }                                 from '../widget-infos';
import { Vector }                                      from '../Vector';
import { HttpResponseUserWidgets, HttpResponseWidget } from '../../Services/AuthServices/User/user.service';

export class WeatherInfos extends WidgetInfos {
  private _cityName: string         = 'Paris';
  private _displayExtremum: boolean = true;

  private _temperature: number;

  constructor() {
    super();
    this.size = new Vector(5, 3);
  }

  public onChangeCity: (string) => void      = null;
  public onToggleExtremum: (boolean) => void = null;

  get cityName(): string {
    return this._cityName;
  }

  set cityName(value: string) {
    this._cityName = value;
  }

  get extremum(): boolean {
    return this._displayExtremum;
  }

  set extremum(value: boolean) {
    this._displayExtremum = value;
  }

  get temperature(): number {
    return this._temperature;
  }

  set temperature(value: number) {
    this._temperature = value;
  }

  fromHttpWidgetModel(model: HttpResponseUserWidgets): WeatherInfos {
    const widget: WidgetInfos = super.fromHttpWidgetModel(model);

    this.uuid = widget.uuid;
    this.position = widget.position;
    this.title = widget.title;

    if (model.options) {
      const opt = JSON.parse(model.options);
      if (opt.cityName) {
        this._cityName = opt.cityName;
      }
      if (opt.extremum) {
        this._displayExtremum = opt.extremum;
      }
    }
    return this;
  }

  public getHttpWidgetModel(): HttpResponseUserWidgets {
    const data = super.getHttpWidgetModel();
    const customOptions = {
      cityName: this._cityName,
      extremum: this._displayExtremum,
    };

    if (data.options) {
      data.options = JSON.stringify(Object.assign(JSON.parse(data.options), customOptions));
    }

    data.widget = {
      id:          3,
      name:        undefined,
      description: undefined,
      options:     undefined,
      createdAt:   undefined,
      updatedAt:   undefined,
      service:     undefined,
    };
    return data;
  }
}

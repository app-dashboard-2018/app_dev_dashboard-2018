import { Injectable }               from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Pokemon, PokemonIdentity } from '../../../Models/Pokemon/pokemon';

@Injectable({
              providedIn: 'root',
            })
export class PokemonService {

  private _pokemonIdentity: Map<string, PokemonIdentity[]>;
  private _pokemonInfos: Map<string, Pokemon>;

  constructor(private http: HttpClient) {
    this._pokemonIdentity = new Map<string, PokemonIdentity[]>();
    this._pokemonInfos    = new Map<string, Pokemon>();
  }

  public nameSearch(widgetId: string, query: string): void {
    this.http
        .get<PokemonIdentity[]>(`http://localhost:3000/pokemon/search?query=${query}`, {observe: 'response'})
        .subscribe((resp: HttpResponse<PokemonIdentity[]>) => {
          this._pokemonIdentity.set(widgetId, resp.body);
        });
  }

  public nameSearchPromise(query: string): Promise<PokemonIdentity[]> {
    return new Promise<PokemonIdentity[]>((resolve, reject) => {
      this.http
          .get<PokemonIdentity[]>(`http://localhost:3000/pokemon/search?query=${query}`, {observe: 'response'})
          .subscribe((resp: HttpResponse<PokemonIdentity[]>) => {
            resolve(resp.body);
          });
    });
  }

  public getSearch(widgetId: string): PokemonIdentity[] {
    return this._pokemonIdentity.get(widgetId) || [];
  }

  public getPokemonInfosById(uuid: string, value: number): Promise<Pokemon> {
    return new Promise((resolve, reject) => {
      this.http
          .get<Pokemon>(`http://localhost:3000/pokemon/pokedex/${value}`, {observe: 'response'})
          .subscribe((resp: HttpResponse<Pokemon>) => {
            resolve(resp.body);
          });
    });
  }

  public getAskedPokemon(uuid: string): Pokemon {
    return this._pokemonInfos.get(uuid);
  }
}

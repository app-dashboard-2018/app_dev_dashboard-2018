import { Injectable } from '@angular/core';
import { interval }   from 'rxjs';
// @ts-ignore
import Timer = NodeJS.Timer;

@Injectable({
              providedIn: 'root',
            })
export class ClockService {
  private _interval: Timer;
  private _now: Date = new Date();

  constructor() {
    this._interval = interval(1000).subscribe((value => {
      this._now = new Date();
    }));
  }

  get now(): Date {
    return this._now;
  }
}

import { Injectable }               from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { TokenService }             from '../Token/token.service';

export interface Token {
  createdAt: Date;
  expiresAt: Date;
  token: string;
}

@Injectable({
              providedIn: 'root',
            })
export class LoginService {

  constructor(private http: HttpClient, private _ts: TokenService) {
  }

  signin(username: string, password: string) {

    return this.http
               .post('http://localhost:3000/login', {username: username, password: password}, {observe: 'response'})
               .subscribe((resp: HttpResponse<Token>) => {
                            this._ts.setToken(resp.body);
                          },
                          (error) => {
                            console.error(error);
                          });
  }

  signup(userData: any) {
    return this.http
               .post('http://localhost:3000/sign-up', userData, {observe: 'response'})
               .subscribe((resp: HttpResponse<any>) => {
                 this.signin(userData.username, userData.password);
               });
  }

  get logged(): boolean {
    return this._ts.isTokenValid();
  }

  logout() {
    this._ts.clearToken();
  }
}

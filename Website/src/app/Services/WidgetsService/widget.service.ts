import { Injectable }                              from '@angular/core';
import { ClockInfos }                              from '../../Models/Time/clock-infos';
import { Layout }                                  from '../../Models/layout';
import { PokedexInfos }                            from '../../Models/Pokemon/pokedex-infos';
import { HttpClient, HttpResponse }                from '@angular/common/http';
import { HttpResponseMe, HttpResponseUserWidgets } from '../AuthServices/User/user.service';
import { WeatherInfos }                            from '../../Models/Weather/weather-infos';
import { PokeTeamInfos }                           from '../../Models/Pokemon/poke-team-infos';
import { CountdownInfos }                          from '../../Models/Time/countdown-infos';

@Injectable({
              providedIn: 'root',
            })
export class WidgetService {

  private _clocks: ClockInfos[]          = [];
  private _pokedex: PokedexInfos[]       = [];
  private _weatherOneDay: WeatherInfos[] = [];
  private _pokeTeams: PokeTeamInfos[]    = [];
  private _countDowns: CountdownInfos[]  = [];

  private _layout: Layout = new Layout();

  constructor(private http: HttpClient) {
  }

  private putInDb(payload: any, cb: (res: HttpResponseUserWidgets) => void = null) {
    this.http.post(`http://localhost:3000/widget/`, payload)
        .subscribe((resp: HttpResponseUserWidgets) => {
          this.http.get(`http://localhost:3000/widget/${resp.uuid}`)
              .subscribe((res: HttpResponseUserWidgets) => {
                if (cb) {
                  cb(res);
                }
              });
        });
  }

  public put(componentType: string): boolean {

    if (componentType === 'clock') {
      this.putInDb({widget: {id: 1}, options: null, positionX: 2, positionY: 2},
                   (res) => {
                     this.updateTimeList(res);
                   });
    } else if (componentType === 'countdown') {
      this.putInDb({widget: {id: 5}, options: null, positionX: 2, positionY: 2},
                   (res) => {
                     this.updateTimeList(res);
                   });
    } else if (componentType === 'pokedex') {
      this.putInDb({widget: {id: 2}, options: null, positionX: 2, positionY: 2},
                   (res) => {
                     this.updatePokemonList(res);
                   });
    } else if (componentType === 'poketeam') {
      this.putInDb({widget: {id: 4}, options: null, positionX: 1, positionY: 1},
                   (res) => {
                     this.updatePokemonList(res);
                   });
    } else if (componentType === 'weather-one-day') {
      this.putInDb({widget: {id: 3}, options: null, positionX: 2, positionY: 2},
                   (res) => {
                     this.updateWeatherList(res);
                   });
    } else {
      return false;
    }
    return true;
  }

  public delete(uuid: string): boolean {

    this._clocks        = this._clocks.filter(item => item.uuid !== uuid);
    this._countDowns    = this._countDowns.filter(item => item.uuid !== uuid);
    this._pokedex       = this._pokedex.filter(item => item.uuid !== uuid);
    this._weatherOneDay = this._weatherOneDay.filter(item => item.uuid !== uuid);
    this._pokeTeams     = this._pokeTeams.filter(item => item.uuid !== uuid);

    this.http.get(`http://localhost:3000/widget/${uuid}/delete`, {observe: 'response'})
        .subscribe((res) => {
        });

    return true;
  }

  public persist(uuid: string): boolean {
    for (const c of this._clocks) {
      if (c.uuid === uuid) {
        this.persistWidget(uuid, c.getHttpWidgetModel());
        return true;
      }
    }

    for (const ct of this._countDowns) {
      if (ct.uuid === uuid) {
        this.persistWidget(uuid, ct.getHttpWidgetModel());
        return true;
      }
    }

    for (const p of this._pokedex) {
      if (p.uuid === uuid) {
        this.persistWidget(uuid, p.getHttpWidgetModel());
        return true;
      }
    }

    for (const pt of this._pokeTeams) {
      if (pt.uuid === uuid) {
        this.persistWidget(uuid, pt.getHttpWidgetModel());
        return true;
      }
    }

    for (const w of this._weatherOneDay) {
      if (w.uuid === uuid) {
        this.persistWidget(uuid, w.getHttpWidgetModel());
        return true;
      }
    }
    return false;
  }

  private updateTimeList(userWidget: HttpResponseUserWidgets) {
    switch (userWidget.widget.name) {
      case 'clock':

        if (!this._clocks.find(value => value.uuid === userWidget.uuid)) {
          const clock = new ClockInfos();
          clock.fromHttpWidgetModel(userWidget);
          this._clocks.push(clock);
        }
        break;
      case 'countdown':

        if (!this._countDowns.find(value => value.uuid === userWidget.uuid)) {
          const countdown = new CountdownInfos();
          countdown.fromHttpWidgetModel(userWidget);
          this._countDowns.push(countdown);
        }
        break;
      default:
        break;
    }
  }

  private updateWeatherList(userWidget: HttpResponseUserWidgets) {

    switch (userWidget.widget.name) {
      case 'weather-one-day':
        if (!this._weatherOneDay.find(value => value.uuid === userWidget.uuid)) {
          const weather = new WeatherInfos();
          weather.fromHttpWidgetModel(userWidget);
          this._weatherOneDay.push(weather);
        }

        break;
      default:
        break;
    }

  }

  private updatePokemonList(userWidget: HttpResponseUserWidgets) {
    switch (userWidget.widget.name) {
      case 'pokedex':

        if (!this._pokedex.find(value => value.uuid === userWidget.uuid)) {
          const pokedex = new PokedexInfos();
          pokedex.fromHttpWidgetModel(userWidget);
          this._pokedex.push(pokedex);
        }
        break;

      case 'poketeam':
        if (!this._pokeTeams.find(value => value.uuid === userWidget.uuid)) {
          const poketeam = new PokeTeamInfos();
          poketeam.fromHttpWidgetModel(userWidget);
          this._pokeTeams.push(poketeam);
        }
        break;

      default:
        break;

    }
  }

  private refreshWidgets(widgets: HttpResponseUserWidgets[]) {
    for (const item of widgets) {
      switch (item.widget.service.name) {

        case 'countdown':
        case 'time':
          this.updateTimeList(item);
          break;

        case 'poketeam':
        case 'pokemon':
          this.updatePokemonList(item);
          break;

        case 'weather':
          this.updateWeatherList(item);
          break;

        default:
          break;
      }

    }
  }

  public refresh(data: HttpResponseMe) {
    const widgets = data.widgets;

    this.refreshWidgets(widgets);
  }

  private persistWidget(uuid: string, payload: any) {
    this.http.post(`http://localhost:3000/widget/${uuid}`, payload, {observe: 'response'})
        .subscribe((resp: HttpResponse<any>) => {
                   },
                   (error) => {
                   });
  }

  get weatherOneDay(): WeatherInfos[] {
    return this._weatherOneDay;
  }

  get clocks(): ClockInfos[] {
    return this._clocks;
  }

  get pokedex(): PokedexInfos[] {
    return this._pokedex;
  }

  get pokeTeams(): PokeTeamInfos[] {
    return this._pokeTeams;
  }

  get layout(): Layout {
    return this._layout;
  }

  get countDowns(): CountdownInfos[] {
    return this._countDowns;
  }

}
